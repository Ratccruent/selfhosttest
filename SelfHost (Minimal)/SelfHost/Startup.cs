﻿using IdentityServer3.Core.Configuration;
using Microsoft.Owin.Security.WsFederation;
using Owin;
using SelfHost.Config;
using Microsoft.IdentityModel.Protocols;

namespace SelfHost
{
    internal class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var factory = new IdentityServerServiceFactory();
            factory
                .UseInMemoryClients(Clients.Get())
                .UseInMemoryScopes(Scopes.Get())
                .UseInMemoryUsers(Users.Get());

            var options = new IdentityServerOptions
            {
                SiteName = "IdentityServer3 (self host)",

                SigningCertificate = Certificate.Get(),
                Factory = factory,

                AuthenticationOptions = { IdentityProviders = ConfigureIdentityProviders }
            };

            appBuilder.UseIdentityServer(options);

        }

        public static void ConfigureIdentityProviders(IAppBuilder app, string signInAsType)
        {
            var metadataAddress = "https://ssoad.adfs.siterra.com/FederationMetadata/2007-06/FederationMetadata.xml";
            var manager = new SyncConfigurationManager(new ConfigurationManager<WsFederationConfiguration>(metadataAddress));

            app.UseWsFederationAuthentication(
                new WsFederationAuthenticationOptions
                {
                    Wtrealm = "urn:identityServerDev",
                    //MetadataAddress = "https://172.31.27.202/is3/core/federationmetadata/2007-06/federationmetadata.xml",
                    //MetadataAddress = "https://ssoad.adfs.siterra.com/FederationMetadata/2007-06/FederationMetadata.xml",
                    ConfigurationManager = manager,
                    AuthenticationType = "adfs",
                    Caption = "ADFS",
                    SignInAsAuthenticationType = signInAsType
                });
        }
    }
}